import { Component, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { Banner } from './banner';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  banners: Banner[] = [];
  id = 1;
  isDeleted = false;
  isChoused = false;
  isPropertiesDisplay = false;
  newBanner: Banner;
  wBanner = '';
  hBanner = '';

  private currentBlockId: string = null;


  getRandomColor() {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  createBanner() {
    this.newBanner = {

      id: this.banners.length + 1,
      width: '300px',
      height: '150px',
      bColor: this.getRandomColor()
    }

    this.banners.push(this.newBanner);
  }

  chooseBanner(id) {
    this.currentBlockId = id;
    let el = document.getElementById(id.toString());
    let dragAndDropEl = el.getElementsByTagName('div')[0].style.transform;
    console.log(dragAndDropEl);
    this.isChoused = true;
    this.isPropertiesDisplay = true;
    let position = el.getBoundingClientRect();
    this.isDeleted = true;
  
  }

  

  deleteBanner(id) {
    this.banners = this.banners.filter(item => item.id !== id);
    this.currentBlockId = null;
    this.isPropertiesDisplay = null;
    this.isDeleted = true;
  }


  reWidthBanner(id,event) {
    if (event.key === "Enter") {
      this.currentBlockId = id;
      let baner = this.banners.find(item => item.id == id);
      baner.width = this.wBanner+'px';
      this.wBanner = '';
    }
    

  }
  reHeightBanner(id,event) {
    if (event.key === "Enter") {
    this.currentBlockId = id;
    let baner = this.banners.find(item => item.id == id);
    baner.height = this.hBanner+'px';
      this.hBanner = '';
    }
  }

  reColorBanner(id) {
    this.currentBlockId = id;
    let baner = this.banners.find(item => item.id == id);
    baner.bColor = this.getRandomColor();
  }

  rePositionXBanner(id) {
    this.currentBlockId = id;
    let baner = this.banners.find(item => item.id == id);
    let el = document.getElementById(id.toString());
    let dragAndDropEl = el.getElementsByTagName('div')[0].style.transform;
    console.log(dragAndDropEl);
  }

}
